const 
    Accounts = require('./accounts'),
    Words    = require('./words');

module.exports = {
    Accounts,
    Words
};