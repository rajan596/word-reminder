
const
    _       = require('lodash');

class WordsModel {

    constructor(opts){
        this.mongoInstance = opts.mongo;
        this.collection    = 'word_meanings';
    }

    addWord(id,wordObj){
        let queryObj = {
            user_id     : this.mongoInstance.getObjectId(id),
            word        : wordObj.word
        },
        dataObj = {
            $set        : {
                user_id     : this.mongoInstance.getObjectId(id),
                word        : wordObj.word,
                updated_at  : new Date()
            },
            $push       : { 
                meanings : wordObj.meaning 
            }
        },
        options = {
            upsert : true
        };

        return this.mongoInstance.updateData(null,this.collection,queryObj,dataObj,options);
    }

    fetchWordsByUserId(user_id) {
        let queryObj = {
            user_id     : this.mongoInstance.getObjectId(user_id)
        };
        return this.mongoInstance.fetchData(null,this.collection,queryObj);
    }

    getMeaning(user_id,word) {
        let queryObj = {
            user_id     : this.mongoInstance.getObjectId(user_id),
            word        : word
        };
        return this.mongoInstance.fetchData(null,this.collection,queryObj);
    }
}

module.exports = WordsModel;