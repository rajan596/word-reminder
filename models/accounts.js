
const
    _       = require('lodash');

class AccountsModel {

    constructor(opts){
        this.mongoInstance = opts.mongo;
        this.collection    = 'accounts_details';
    }

    getUserDataByEmailOrMobile(email,mobile){
        let queryObj;

        if(mobile) { // find by either mobile or email
            queryObj = {
                $or : [
                    {"mobile"   : mobile},
                    {"email"    : email}
                ]
            };
        }   
        else {
            queryObj = {
                "email"     : email
            };
        }
        return this.mongoInstance.fetchData(null,this.collection,queryObj);
    }

    getUserDataByEmail(_email) {
        let queryObj = {
            email     : _email
        };
        return this.mongoInstance.fetchData(null,this.collection,queryObj);
    }

    getUserDataById(id){
        let queryObj = {
            _id     : this.mongoInstance.getObjectId(id)
        };
        return this.mongoInstance.fetchData(null,this.collection,queryObj);
    }

    addUser(data){
        let dataObj = {};

        if(_.get(data,'email',null))    dataObj.email       = _.get(data,'email', null);
        if(_.get(data,'mobile',null))   dataObj.mobile      = _.get(data,'mobile', null);
        if(_.get(data,'password',null)) dataObj.password    = _.get(data,'password', null);
        if(_.get(data,'name',null))     dataObj.name        = _.get(data,'name', null);

        return this.mongoInstance.insertData(null,this.collection,dataObj);
    }
}

module.exports = AccountsModel;