var 
    JWT_TOKEN   = require('../lib/jwtToken'),
    jwtTokenLib = new JWT_TOKEN();

function routes(app,controller){
    
    app.get('/_status',function(req,res){
        return res.send("OK");
    });

    app.post('/accounts/v1/signup',function(req,res) {
        controller.signup(req,res);
    });

    app.post('/accounts/v1/login',function(req,res) {
        controller.login(req,res);
    });

    app.post('/words/v1/add',jwtTokenLib.validateLogin,function(req,res) {
        controller.addWord(req,res);
    });

    app.get('/words/v1/fetchAll',jwtTokenLib.validateLogin,function(req,res) {
        controller.fetchAllWords(req,res);
    });

    app.get('/words/v1/getMeaning',jwtTokenLib.validateLogin,function(req,res) {
        controller.getMeaning(req,res);
    });
}



module.exports = routes;