
const 
    jwt     = require('jsonwebtoken'),
    config  = require('../config');


class JwtToken{
    constructor(){
        
    }

    generateToken(userid){
        return jwt.sign({id : userid} , config.data.secret, {
            expiresIn : config.data.tokenExpireTime // in seconds
        });
    }

    verifyToken(token){
        var valid = false;
        jwt.verify(token,config.data.secret, function(err){
            if(err){
                console.error("JwtToken :: verifyToken","Invalid Token");
            }
            else {
                valid = true;
            }
        });

        return valid;
    }

    validateLogin(req,res,next){
        let token = req.headers['x-access-token'] || null;
        jwt.verify(token,config.data.secret, function(err){
            if(err){
                res.status(400).json({"message" : "Invalid Token"});
            }
            else {
                next();
            }
        });
    }
}

module.exports = JwtToken;