/**
 * Common MongoDB Library
 */

let
    Q           = require('q'),

    MongoClient = require('mongodb').MongoClient,
    ObjectID    = require('mongodb').ObjectID;

class MongoUtils {
    constructor(opts){
        this.CONFIG         = opts;
        this.mongoClient    = MongoClient;
        this.db             = null;
        this.connectionUrl  = `mongodb://${this.CONFIG.username}:${this.CONFIG.password}@${this.CONFIG.host}:${this.CONFIG.port}/${this.CONFIG.db}?${this.CONFIG.params}`;
    }

    connect(cb){
        var
            self = this;

        this.mongoClient.connect(this.connectionUrl,{ useNewUrlParser: true } ,function(err,db){
            if(err) {
                cb(err);
            }
            else {
                self.db = db.db(self.CONFIG.db);
                cb(null);
            }
        });
    }

    fetchData(cb,collection,query){
        var deferred = Q.defer();
        var collection = this.db.collection(String(collection));
        collection.find(query).toArray(function(err,result){
            if(err) deferred.reject(err);
            else deferred.resolve(result);
        });
        return deferred.promise;
    }

    insertData(cb,collection,query){
        var deferred = Q.defer();
        this.db.collection(String(collection)).insertOne(query,function(err,result){
            if(err) deferred.reject(err);
            else deferred.resolve(result);
        });
        return deferred.promise;
    }

    insertMultipleData(cb,collection,query){
        var deferred = Q.defer();
        this.db.collection(String(collection)).insertMany(query,function(err,result){
            if(err) deferred.reject(err);
            else deferred.resolve(result);
        });
        return deferred.promise;
    }

    updateData(cb,collection,query,data,option){
        var deferred = Q.defer();
        this.db.collection(String(collection)).updateOne(query,data,option,function(err,result){
            if(err) deferred.reject(err);
            else deferred.resolve(result);
        });
        return deferred.promise;
    }

    updateMultipleData(cb,collection,query,data,option){
        var deferred = Q.defer();
        this.db.collection(String(collection)).updateMany(query,data,option,function(err,result){
            if(err) deferred.reject(err);
            else deferred.resolve(result);
        });
        return deferred.promise;
    }

    getObjectId(id) {
        return new ObjectID(id);
    }

    close(){
        this.db.close();
    }
}

module.exports = MongoUtils;