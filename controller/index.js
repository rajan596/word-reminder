

const 
    ACCOUNTS_MODEL      = require('../models').Accounts, 
    ACCOUNT_MANAGEMENT  = require('./accounts'),
    WORDS_MANAGEMENT     = require('./wordManagement');

class Controller{
    constructor(opts){
        this.accountManagement = new ACCOUNT_MANAGEMENT(opts);
        this.wordsManagement = new WORDS_MANAGEMENT(opts);
    }

    signup(req,res){
        this.accountManagement.signupManager.processSignup(req,res);
    }

    login(req,res){
        this.accountManagement.loginManager.processLogin(req,res);
    }

    addWord(req,res){
        this.wordsManagement.addWordsManager.addWord(req,res);
    }

    fetchAllWords(req,res){
        this.wordsManagement.fetchWordsManager.fetchAllWords(req,res);
    }

    getMeaning(req,res) {
        this.wordsManagement.fetchWordsManager.getMeaning(req,res);
   }
}

module.exports = Controller;