
const
    _               = require('lodash'),
    WORDS_MODEL   = require('../../models').Words,

    Q               = require('q');

class AddWordsManager {
    constructor(opts){
        this.CONFIG         = opts.config;
        this.wordsModel     = new WORDS_MODEL(opts);
    }

    addWord(req,res) {
        let 
            self             = this,
            wordDetails      = _.get(req,'body.word_details',null),
            user_id          = _.get(req,'headers.id',null),
            validationResult = this.validateRequest(wordDetails,user_id),
            response         = { // assigning default values
                statCode : 500,
                message : self.CONFIG.MESSAGES.DEFAULT_FAILURE
            };

        if(validationResult.statCode!=200) {
            console.error("AddWordManager :: addWord","invalid request",req.body);
            return res
                    .status(validationResult.statCode)
                    .json({ 
                        "statCode" : validationResult.statCode,
                        "message" : validationResult.message
                    });
        }

        Q(undefined)
        .then(function(){
            // comverting word to lowercase
            wordDetails.word = wordDetails.word.toLowerCase();
            return self.wordsModel.addWord(user_id,wordDetails);
        })
        .then(function(result){
            console.log("AddWordManager :: addWord","Successful for word", wordDetails);
            response.statCode = 200;
            response.message  = self.CONFIG.MESSAGES.ADD_WORD_SUCCESS;

            return res.status(response.statCode).json(response);
        })
        .catch(function(err){
            console.log("AddWordManager :: addWord","error while processing request",req.body,err);
            return res.status(response.statCode).json(response);
        });
    }

    validateRequest(wordDetails,id) {
        let 
            response        = {statCode : 200},
            mandatoryFiels  = _.get(this.CONFIG,'data.addWordMandatoryFields',[]);

        if(!id) {
            response = {
                statCode    : 400,
                message     : 'Invalid id' 
            };
        }
        else if(_.isEmpty(wordDetails)) {
            response = {
                statCode    : 400,
                message     : 'Empty Body' 
            };
        }
        else {
            mandatoryFiels.forEach(function(field){
                if(!_.get(wordDetails,[field],null)) {
                    response =  {
                        statCode    : 400,
                        message     : 'Mandatory field ' + field + ' missing in word_details'
                    };
                }
            });
        }

        return response;
    }
}

module.exports = AddWordsManager;