const 
    ADD_WORDS_MANAGER   = require('./addWord'),
    FETCH_WORDS_MANAGER = require('./fetchWords');

class WordsManager{
    constructor(opts){
        this.addWordsManager    = new ADD_WORDS_MANAGER(opts);
        this.fetchWordsManager  = new FETCH_WORDS_MANAGER(opts);
    }
}

module.exports = WordsManager;