
const
    _               = require('lodash'),
    WORDS_MODEL   = require('../../models').Words,

    Q               = require('q');

class FetchWordManager {
    constructor(opts){
        this.CONFIG         = opts.config;
        this.wordsModel     = new WORDS_MODEL(opts);
    }

    fetchAllWords(req,res) {
        let 
            self             = this,
            user_id          = _.get(req,'headers.id',null),
            validationResult = this.validateRequest(user_id),
            response         = { // assigning default values
                statCode : 500,
                message : self.CONFIG.MESSAGES.DEFAULT_FAILURE
            };

        if(validationResult.statCode!=200) {
            console.error("FetchWordManager :: fetchAllWords","invalid request",req.body);
            return res
                    .status(validationResult.statCode)
                    .json({ 
                        "statCode" : validationResult.statCode,
                        "message" : validationResult.message
                    });
        }

        Q(undefined)
        .then(function(){
            return self.wordsModel.fetchWordsByUserId(user_id);
        })
        .then(function(result){
            console.log("FetchWordManager :: fetchAllWords","Successful for word");
            // remove unwanted keys from result array
            response.words    = _.map(result, function(obj) { return _.pick(obj,['word','meanings','updated_at']);}); 
            response.statCode = 200;
            response.message  = self.CONFIG.MESSAGES.DEFAULT_SUCCESS;

            return res.status(response.statCode).json(response);
        })
        .catch(function(err){
            console.log("FetchWordManager :: fetchAllWords","error while processing request",req.body,err);
            return res.status(response.statCode).json(response);
        });
    }

    getMeaning(req,res){
        let 
            self             = this,
            user_id          = _.get(req,'headers.id',null),
            word             = _.get(req,'query.word',null),
            validationResult = this.validateRequestForGetMeanning(user_id,word),
            response         = { // assigning default values
                statCode : 500,
                message : self.CONFIG.MESSAGES.DEFAULT_FAILURE
            };

        if(validationResult.statCode!=200) {
            console.error("FetchWordManager :: getMeaning","invalid request",req.body);
            return res
                    .status(validationResult.statCode)
                    .json({ 
                        "statCode" : validationResult.statCode,
                        "message" : validationResult.message
                    });
        }

        Q(undefined)
        .then(function(){
            // converting word to lowercase
            word = word.toLowerCase();
            return self.wordsModel.getMeaning(user_id,word);
        })
        .then(function(result){
            if(_.get(result,'length',0)){
                return Q.resolve(result[0]);
            }
            else {
                response = {
                    statCode : 403,
                    message    : self.CONFIG.MESSAGES.NO_WORD_FOUND
                }; 
                return Q.reject(response.message);
            }
        })
        .then(function(result){

            console.log("FetchWordManager :: getMeaning","meaning found for word",word);
            // remove unwanted keys from result array
            response.meanings = _.get(result,['meanings'],[]); 
            response.statCode = 200;
            response.message  = self.CONFIG.MESSAGES.DEFAULT_SUCCESS;

            return res.status(response.statCode).json(response);
        })
        .catch(function(err){
            console.log("FetchWordManager :: getMeaning","error while processing for word",word);
            return res.status(response.statCode).json(response);
        });
    }

    validateRequest(id) {
        let 
            response        = {statCode : 200};

        if(!id) {
            response = {
                statCode    : 400,
                message     : 'Invalid id' 
            };
        }

        return response;
    }

    validateRequestForGetMeanning(user_id,word){
        let 
            response        = {statCode : 200};

        if(!user_id) {
            response = {
                statCode    : 400,
                message     : 'Invalid id' 
            };
        }
        else if(!word) {
            response = {
                statCode    : 400,
                message     : 'Word param missing' 
            };
        }

        return response;
    }
}

module.exports = FetchWordManager;