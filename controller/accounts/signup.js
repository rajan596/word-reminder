
const
    _               = require('lodash'),
    ACCOUNT_MODEL   = require('../../models').Accounts,
    
    Q               = require('q');

class SignUpManager {
    constructor(opts){
        this.CONFIG       = opts.config;
        this.accountsModel = new ACCOUNT_MODEL(opts);
    }

    processSignup(req,res) {
        let 
            self             = this,
            validationResult = this.validateRequest(req),
            form             = _.get(req,'body.form',null),
            response         = { // assigning default values
                statCode : 500,
                message : self.CONFIG.MESSAGES.signup.statcode["500"]
            },
            collectedData = {}

        if(validationResult.statCode!=200) {
            console.error("SignUpManager :: processSignup","invalid request",req.body);
            return res
                    .status(validationResult.statCode)
                    .json({ 
                        "statCode" : validationResult.statCode,
                        "message" : validationResult.message
                    });
        }

        collectedData = self.getCollectedData(req);

        Q(undefined)
        .then(function(){
            // check if data with given mobile and email already exists ?
            let 
                email   = _.get(form,'email.value',null),
                mobile  = _.get(form,'mobile.value',null);            
            return self.accountsModel.getUserDataByEmailOrMobile(email,mobile);
        })
        .then(function(result){
            if(_.get(result,'length',0)) { // user found reject request
                response = {
                    statCode : 406,
                    message  : self.CONFIG.MESSAGES.signup.statcode["406"]
                };
                return Q.reject(response.message);
            }
            else {
                return Q.resolve();
            }
        })
        .then(function(){
            // insert user details 
            return self.accountsModel.addUser(collectedData);
        })
        .then(function(result){
            console.log("User added successfully", form);
            response.statCode = 200;
            response.message  = self.CONFIG.MESSAGES.signup.statcode["200"];
            response.id       = _.get(result,'insertedId',null);

            return res.status(response.statCode).json(response);
        })
        .catch(function(err){
            console.log("SignUpManager :: processSignup","error while processing request",req.body,err);
            return res.status(response.statCode).json(response);
        });
    }

    validateRequest(req) {
        let 
            response        = {statCode : 200},
            form            = _.get(req,'body.form',null),
            mandatoryFiels  = _.get(this.CONFIG,'data.signUpMandatoryFields',[]);

        if(_.isEmpty(form)) {
            response = {
                statCode    : 400,
                message     : 'Empty form' 
            };
        }
        else {
            mandatoryFiels.forEach(function(field){
                if(!_.get(form,[field,'value'],null)) {
                    response =  {
                        statCode    : 400,
                        message     : 'Mandatory field ' + field + ' missing in form'
                    };
                }
            });
        }

        return response;
    }

    getCollectedData(req) {
        let form = _.get(req,'body.form',{});
        return {
            name        : _.get(form,'name.value',null),
            mobile      : _.get(form,'mobile.value',null),
            email       : _.get(form,'email.value',null),
            password    : _.get(form,'password.value',null)
        };
    }
}

module.exports = SignUpManager;