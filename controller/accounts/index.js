const 
    SIGNUP_MANAGER   = require('./signup'),
    LOGIN_MANAGER    = require('./login');

class AccountManager{
    constructor(opts){
        this.signupManager = new SIGNUP_MANAGER(opts);
        this.loginManager  = new LOGIN_MANAGER(opts);
    }
}

module.exports = AccountManager;