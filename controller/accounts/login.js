
const
    _               = require('lodash'),
    ACCOUNT_MODEL   = require('../../models').Accounts,

    Q               = require('q'),
    JWT_TOKEN       = require('../../lib/jwtToken');

class LoginManager {
    constructor(opts){
        this.CONFIG         = opts.config;
        this.accountsModel  = new ACCOUNT_MODEL(opts);
        this.jwtTokenLib    = new JWT_TOKEN();
    }

    processLogin(req,res) {
        let 
            self             = this,
            validationResult = this.validateRequest(req),
            form             = _.get(req,'body.form',null),
            response         = { // assigning default values
                statCode : 500,
                message : self.CONFIG.MESSAGES.LOGIN_FAILURE
            };

        if(validationResult.statCode!=200) {
            console.error("LoginManager :: processLogin","invalid request",req.body);
            return res
                    .status(validationResult.statCode)
                    .json({ 
                        "statCode" : validationResult.statCode,
                        "message" : validationResult.message
                    });
        }

        Q(undefined)
        .then(function(){
            // fetch data for given email 
            let email = _.get(form,'email.value',null);
            return self.accountsModel.getUserDataByEmail(email);
        })
        .then(function(result){
            if(_.get(result,'length',0)) {  // record found
                if(_.get(result,'0.password') == _.get(form,'password.value')) { // credential matches
                    response.id = _.get(result,'0._id',null);
                    return Q.resolve();
                }
                else {
                    response = {
                        statCode : 403,
                        message  : self.CONFIG.MESSAGES.INVALID_PASSWORD
                    };
                    return Q.reject(response.message);
                }
            }
            else { // no record exists
                response = {
                    statCode : 403,
                    message  : self.CONFIG.MESSAGES.INVALID_CREDENTIALS
                };
                return Q.reject();
            }
        })
        .then(function(result){
            console.log("Login Successful for email", _.get(form,'email.value'));
            response.statCode = 200;
            response.message  = self.CONFIG.MESSAGES.LOGIN_SUCCESS;
            response.token    = self.jwtTokenLib.generateToken(_.get(form,'email.value',null));

            return res.status(response.statCode).json(response);
        })
        .catch(function(err){
            console.log("LoginManager :: processLogin","error while processing request",req.body,err);
            return res.status(response.statCode).json(response);
        });
    }

    validateRequest(req) {
        let 
            response        = {statCode : 200},
            form            = _.get(req,'body.form',null),
            mandatoryFiels  = _.get(this.CONFIG,'data.loginMandatoryFields',[]);

        if(_.isEmpty(form)) {
            response = {
                statCode    : 400,
                message     : 'Empty form' 
            };
        }
        else {
            mandatoryFiels.forEach(function(field){
                if(!_.get(form,[field,'value'],null)) {
                    response =  {
                        statCode    : 400,
                        message     : 'Mandatory field ' + field + ' missing in form'
                    };
                }
            });
        }

        return response;
    }
}

module.exports = LoginManager;