const 
    express     = require('express'),
    app         = express(),
    bodyParser  = require('body-parser'),

    CONFIG      = require('./config'),
    ROUTES      = require('./routers'),
    CONTROLLER  = require('./controller'),
    MONGO       = require('./lib/mongoUtil'),
    mongoClient = new MONGO(CONFIG.MONGO),
    PORT        = 5959;

let 
    options         = {
        config  : CONFIG,
        mongo   : mongoClient
    },
    controllerObj   = new CONTROLLER(options);

// parses applicaton/json body into req.body
app.use(bodyParser.json());



ROUTES(app,controllerObj);

mongoClient.connect(function(err){

    if(err) {
        console.error("index ::","error while connecting to mongodb",err);
        process.exit(0);
    }
    else {
        console.log("index :: ","MongoDB connected with word reminder db");
        app.listen(PORT, function (){
            console.log(`Server Started on Port ${PORT}`);
        });
    }
});

