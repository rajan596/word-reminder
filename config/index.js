
const
    env     = (process.env.NODE_ENV || 'development').toLowerCase();
    mongo   = require('./mongo'),
    message = require('./messages'),
    data    = require('./data');

module.exports = {
    MONGO       : mongo[env],
    MESSAGES    : message ,
    data        : data
};