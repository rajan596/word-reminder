
return module.exports = {
    'signup' : {
        'statcode' : {
            '200' : 'Successfully Registered',
            '400' : 'Bad Request',
            '401' : 'Authentication Failure',
            '406' : 'Email or Mobile already exists',
            '500' : 'Some error occured, please try after some time'
        }
    },
    'LOGIN_FAILURE'     : 'Some error occured, please try after some time',
    'INVALID_PASSWORD'  : 'Incorrect password',
    'INVALID_CREDENTIALS' : 'Invalid credentials',
    'LOGIN_SUCCESS'     : 'login successful',
    'DEFAULT_FAILURE'   : 'Some error occured, please try after some time',
    'DEFAULT_SUCCESS'   : 'Success!',
    'ADD_WORD_SUCCESS'  : 'Success',
    'NO_WORD_FOUND'     : 'No word found '
};