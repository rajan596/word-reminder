
module.exports = {
    'signUpMandatoryFields' : ['password','email'],
    'loginMandatoryFields'  : ['password','email'],
    'addWordMandatoryFields': ['word','meaning'],
    'secret'                : 'WestWorld',
    'tokenExpireTime'       : 86400 // value = (total hours)*(3600) seconds
};